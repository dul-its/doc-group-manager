.. _library-group-policies:

Use of Groups in the Libraries
==============================

While how to use Group Manager should now be clear, [#confused]_ there
are some guidelines to follow to ensure consistent use of groups
throughout the Libraries.

.. _natural-group-guidelines:

Guidelines
----------

* Group display names should follow the form:
  lib-[section-abbreviation]-[department-or-project]. Using all
  lowercase is recommended.

* Department and project managers should identify groups that need to
  be created, and work with ITS support staff create and learn how to
  keep them updated.

* Add the `lib-its-support support group`_ as an owner of all groups
  for which you may want help.

* Enable the `Sync to WIN Active Directory` and `Make Public in
  Grouper` group options. This allows the group to be used via the
  other systems of groups on campus. [#private]_

An example of a group that follows these guidelines is the group for
Desktop Support, lib-its-desktop-support. The name follows the form
above, where lib is always lib, the section abbreviation is its,
showing that this group relates to something in Information Technology
Services, and the department or project is desktop-support. Dorothy
Coletta is an owner of the group since she is manager of Desktop
Support. lib-its-support is also an owner of the group so that the
rest of the ITS support staff can provide assistance maintaining the
group if the need arises.

.. _lib-its-support support group: https://groups.oit.duke.edu/groupmanager/group?id=c1eb950036d64f81a9e3b2e919cb21f2

Application Specific Groups
---------------------------

In addition to the groups described above, which represent groups of
people in the real world outside the context of computer systems,
application administrators will sometimes create groups specific to
their applications. Examples are lib-dpc-repo-archive-read and
lib-digital-collections-previewers. The purpose of this type of group
is to make it easier to manage changes to groups those
applications. For example, granting read permissions on the DPC
archival filesystem would mean changed the permissions on thousands of
individual files. Instead, the application administrator will create
one group that will be given, say, editing permissions that will be
set up automatically get editing permissions on all the files. Then,
when a new group of people need editing permissions, that group can be
added to the editors group created by the application administrators
in a single action.

The guidelines for these application specific groups are:

* Once created, ownership of these groups should be granted to the
  functional owner for application in which they are used.

* Normally, membership in these groups will be other groups as
  described in :ref:`natural-group-guidelines`.

.. rubric:: Footnotes

.. [#confused] If it is not, then it is probably a problem with this
	       documentation. Please let us know so that we can
	       improve the documentation or work with OIT to improve
	       the tool.

.. [#private] Don't do this if the group name or membership should not
	      be public knowledge. 
