Welcome to Library Group Manager Use and Policies documentation!
================================================================

`Group Manager <https://groups.oit.duke.edu/groupmanager/>`_ is Duke's
tool for creating and managing groups of computer accounts.

Why Do Groups Matter
--------------------

Groupings of people occur naturally. For example, everyone who is in
the same department or working on the same project. Often, these
groups of people will need access to the same electronic resource or
need to be presented with the same options in an application. Without
a scheme to encode groups for computer use, permissions and
configurations would have to be applied to individuals across a
variety of applications. This process would have to be repeated every
time the permissions [#roles]_ or groups needed to be changed. By
managing groups in a central place this tedium can be
avoided. [#nogroups]_ In addition, it is easier to understand an
access list, for example, that is populated with meaningful groups
than one that is a list of individuals.

Group Manager in Context
------------------------

Group Manager is a tool that facilitates the creation and maintenance
of groups by providing an interface and a model (groups have members
and owners, see :ref:`use-documentation`). Being able to model groups
is great, but groups really shine when applications are able to use
them. Some applications, like `AKA <https://aka.oit.duke.edu/>`_, are
able to use Group Manager groups directly. AKA is special in that it
was written specifically for a Duke audience after Group Manger
existed. Many applications, however, are not able to make us of Group
Manager directly, but are able to make use of other systems of groups
such as LDAP, Grouper [#gmg]_, Active Directory, Sympa, etc. To
facilitate the use of groups by applications that do not have support
for Group Manager, Group Manager groups can propagate to these other
systems. The recommended way (see :ref:`library-group-policies`) to
deal with the multiplicity of systems of groups is to always manage
groups using Group Manager. That way, all groups can be managed in one
place, and can appear in any of the other systems of groups when needed.

Contents
--------

.. toctree::
   :maxdepth: 2

   use
   policies


.. rubric:: Footnotes

.. [#roles] Grouping permissions together is also useful. For example,
	    it does not make sense for someone to be able to edit a
	    resource, but not see that it exists.  Therefore the view
	    and edit permissions might be grouped together and that
	    group of permissions be granted to people who are expected
	    to be editors. These groups of permissions are often
	    called roles and the whole scheme role based access
	    control. This document does not deal with this type of
	    groups, but rather groups of computer users.

.. [#nogroups] Unfortunately, not all applications in use at Duke
	      have support for an external source of groups, so not
	      all tedium can be avoided.

.. [#gmg] The relationship between Group Manager and Grouper is
	   more complicated than between Group Manager and the other
	   systems of groups. Group Manager actually uses Grouper as
	   the store for its groups. However, interaction with these
	   groups through Grouper is restricted in order to maintain
	   the Group Manager invariants. Therefore, it is useful to
	   think about them as separate systems.
