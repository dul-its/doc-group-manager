Library Group Manager Use and Policies
======================================

This repository contains documentation for the use of Group Manager in
the Libraries. It is maintained using the Sphinx_ documentation
system. The documentation can be found in the ``doc`` directory along
with a Makefile for producing various output formats.

.. _Sphinx: http://www.sphinx-doc.org/en/stable/
